<?php

class CashflowTableSeeder extends Seeder {

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        //\DB::table('artikel')->truncate();

        \DB::table('cashflow')->insert(array (
            array (
                'kode' => '1',
                'kode_head' => '-',
                'keterangan' => 'Aktiva',
                'posisi' => 'D',
                'fungsi' => 'neraca',
                'view' => 0,
                'urutan' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ),
            array (
                'kode' => '1.1',
                'kode_head' => '1',
                'keterangan' => 'Kas Tunai',
                'posisi' => 'D',
                'fungsi' => 'neraca',
                'view' => 1,
                'urutan' => 2,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ),
            array (
                'kode' => '1.2',
                'kode_head' => '1',
                'keterangan' => 'Bank',
                'posisi' => 'D',
                'fungsi' => 'neraca',
                'view' => 0,
                'urutan' => 3,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ),
            array (
                'kode' => '1.2.1',
                'kode_head' => '1.2',
                'keterangan' => 'Bank BCA',
                'posisi' => 'D',
                'fungsi' => 'neraca',
                'view' => 0,
                'urutan' => 4,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ),
            array (
                'kode' => '2',
                'kode_head' => '-',
                'keterangan' => 'Passiva',
                'posisi' => 'K',
                'fungsi' => 'neraca',
                'view' => 0,
                'urutan' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ),
            array (
                'kode' => '2.1',
                'kode_head' => '2',
                'keterangan' => 'Deposito Agen',
                'posisi' => 'K',
                'fungsi' => 'neraca',
                'view' => 1,
                'urutan' => 2,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ),
            array (
                'kode' => '2.2',
                'kode_head' => '2',
                'keterangan' => 'Hutang',
                'posisi' => 'K',
                'fungsi' => 'neraca',
                'view' => 0,
                'urutan' => 3,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ),
            array (
                'kode' => '2.2.1',
                'kode_head' => '2.2',
                'keterangan' => 'Hutang Tiket',
                'posisi' => 'K',
                'fungsi' => 'neraca',
                'view' => 1,
                'urutan' => 4,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ),
        ));
    }

}
