<?php

use Timenz\Cashflow\CashflowTable;
use Timenz\Cashflow\CashflowModel;

class CashflowController extends Controller {

    protected $cashflow;
    protected $cashflowModel;

    function __construct(CashflowTable $cashflow, CashflowModel $cashflowModel){
        $this->cashflow = $cashflow;
        $this->cashflowModel = $cashflowModel;

        //parent::__construct();

    }


    public function jurnalInput(){
        //$this->layout = null;
        $input = Input::all();

        $n = count($input['kode-debet']);

        if($n < count($input['kode-kredit'])){
            $n = count($input['kode-kredit']);
        }

        $data_jurnal = array();
        $data_kode = array();
        $sum_debet = 0;
        $sum_kredit = 0;

        for($i = 0; $i < $n; $i++){
            $kode_debet = $this->cashflowModel->getStringBetween($input['kode-debet'][$i], '(', ')');
            if($kode_debet != '' and $input['nom-debet'][$i] > 0){
                $data_jurnal[] = array('kode' => $kode_debet, 'nominal' => $input['nom-debet'][$i], 'posisi' => 'D');
                if(!in_array($kode_debet, $data_kode)){
                    $data_kode[] = $kode_debet;
                }

                $sum_debet += $input['nom-debet'][$i];

            }

            $kode_kredit = $this->cashflowModel->getStringBetween($input['kode-kredit'][$i], '(', ')');
            if($kode_kredit != '' and $input['nom-kredit'][$i] > 0){
                $data_jurnal[] = array('kode' => $kode_kredit, 'nominal' => $input['nom-kredit'][$i], 'posisi' => 'K');
                if(!in_array($kode_kredit, $data_kode)){
                    $data_kode[] = $kode_kredit;
                }

                $sum_kredit += $input['nom-kredit'][$i];

            }

        }

        if(count($data_kode) > 0){
            $aff = $this->cashflow->whereIn('kode', $data_kode)->get();
            if(count($aff) != count($data_kode)){
                return;
            }


        }

        $jurnal = $this->cashflowModel->inputJurnal($data_jurnal, $sum_debet, $input['keterangan'], Input::get('id_user'));

        return Redirect::back();


    }

}
