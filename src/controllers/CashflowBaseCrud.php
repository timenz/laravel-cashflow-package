<?php

use Timenz\Crud\Crud;

class BaseCrudData{

}

class CashflowBaseCrud extends Crud{


    private $masterData;
    protected $masterBlade = 'ok';
    protected $viewData;

    public function __construct(){
        $this->masterData = new BaseCrudData();
    }

    protected function run(){
        $this->init('cashflow');
        $this->orderBy('urutan', 'asc');
        $this->title = 'Cashflow Akun';

        $this->changeType('last_update', 'hidden', date('Y-m-d H:i:s'), true);
        $this->changeType('fungsi', 'enum', array('neraca', 'labarugi'));
        $this->changeType('posisi', 'select', array('D' => 'Debet', 'K' => 'Kredit'));
        $this->changeType('view', 'select', array('0' => 'Hidden', '1' => 'Show'));

        $this->setMasterBlade($this->masterBlade);

        $this->viewData['cashflow'] = $this->masterData;


        $this->setMasterData($this->viewData);
        return true;
    }
}