<?php  namespace Timenz\Cashflow;
use Eloquent;

class CashflowJurnalDebet extends Eloquent{
    protected $fillable = array('kode_jurnal', 'kode', 'nominal');
    protected $table = 'cashflow_jurnal_debet';
    public $timestamps = false;

}