<?php namespace Timenz\Cashflow;

use Eloquent;

class CashflowTable extends Eloquent{

    protected $table = 'cashflow';

    public $timestamps = false;


}