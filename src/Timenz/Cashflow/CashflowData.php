<?php namespace Timenz\Cashflow;

use Input;
use Config;
use Route;

class CashflowData {


    private $masterBlade = '';
    private $idUser = 0;
    private $view_path = 'packages.timenz.cashflow.';
    private $cashflow;
    private $cashflowJurnal;

    public function __construct(){
        $this->cashflow = new CashflowTable();
        $this->cashflowJurnal = new CashflowJurnal();
    }

    public function setMasterBlade($masterBlade){
        $this->masterBlade = $masterBlade;
    }
    public function setIdUser($idUser){
        $this->idUser = $idUser;
    }

    public function jurnal(){
        $cashflow = $this->cashflow
            ->where('view', 1)
            ->where('fungsi', 'neraca')
            ->orderBy('urutan', 'asc')->get();

        $debet = array();
        $kredit = array();
        $kodeJurnal = array();

        foreach($cashflow as $item){
            if($item->posisi == 'D'){
                $debet[] = '('.$item->kode.') '.$item->keterangan;
            }else{
                $kredit[] = '('.$item->kode.') '.$item->keterangan;
            }
            $kodeJurnal[] = '('.$item->kode.') '.$item->keterangan;
        }

        $data = array(
            'debet' => json_encode($debet),
            'kredit' => json_encode($kredit),
            'kodeJurnal' => json_encode($kodeJurnal),
            'view_path' => $this->view_path,
            'master_blade' => $this->masterBlade,
            'id_user' => $this->idUser,
            'view' => $this->view_path.'jurnal',
            'action' => Route::getCurrentRequest()->decodedPath(),
        );

        return $data;
    }

    public function neraca(){
        $tanggal_ori = Input::get('tanggal');
        $tanggal = strtotime($tanggal_ori);

        $dataNeraca = array();
        $dataIsEmpty = true;

        if($tanggal > 0){
            $dataNeraca = $this->getNeraca($tanggal);

            if(count($dataNeraca) > 0){
                $dataIsEmpty = false;
            }
            if(count($dataNeraca['tabel']) < 1){
                $dataIsEmpty = true;
            }
        }

        $array = array(
            'view_path' => $this->view_path,
            'action' => Route::getCurrentRequest()->decodedPath(),
            'master_blade' => $this->masterBlade,
            'view' => $this->view_path.'neraca',
            'data_is_empty' => $dataIsEmpty,
            'tanggal' => $tanggal_ori,
            'data_neraca' => $dataNeraca
        );

        return $array;
    }

    public function akun(){
        $tanggalOri = Input::get('tanggal');
        $tanggalKeOri = Input::get('tanggal_ke');
        $kodeAkun = Input::get('kode_akun');
        $tanggal = strtotime($tanggalOri);
        $tanggalKe = strtotime($tanggalKeOri);

        $dataAkun = array();
        $dataIsEmpty = true;

        if($tanggal > 0){
            $dataAkun = $this->getAkun($tanggal, $tanggalKe);
            //$dataHarian = array();

            if(count($dataAkun) > 0){
                $dataIsEmpty = false;
                if(count($dataAkun['tabel']) < 1){
                    $dataIsEmpty = true;
                }
            }

        }

        $array = array(
            'data_is_empty' => $dataIsEmpty,
            'tanggal' => $tanggalOri,
            'tanggal_ke' => $tanggalKeOri,
            'data_harian' => $dataAkun,
            'view_path' => $this->view_path,
            'master_blade' => $this->masterBlade,
            'view' => $this->view_path.'harian',
            'action' => Route::getCurrentRequest()->decodedPath(),
        );

        return $array;
    }

    private function getAkun(){
        return false;
    }

    public function labaRugi(){

        $tanggal_ori = Input::get('tanggal');
        $tanggal = strtotime($tanggal_ori);

        $dataLabaRugi = array();
        $dataIsEmpty = true;

        if($tanggal > 0){
            $dataLabaRugi = $this->getLabaRugi($tanggal);

            if(count($dataLabaRugi) > 0){
                $dataIsEmpty = false;
            }
            if(count($dataLabaRugi['tabel']) < 1){
                $dataIsEmpty = true;
            }
        }

        $array = array(
            'data_is_empty' => $dataIsEmpty,
            'tanggal' => $tanggal_ori,
            'data_labarugi' => $dataLabaRugi,
            'view_path' => $this->view_path,
            'master_blade' => $this->masterBlade,
            'view' => $this->view_path.'labarugi',
            'action' => Route::getCurrentRequest()->decodedPath(),
        );

        return $array;
    }



    public function harian(){

        $tanggalOri = Input::get('tanggal');
        $tanggalKeOri = Input::get('tanggal_ke');
        $tanggal = strtotime($tanggalOri);
        $tanggalKe = strtotime($tanggalKeOri);

        $dataHarian = array();
        $dataIsEmpty = true;

        if($tanggal > 0){
            $dataHarian = $this->getHarian($tanggal, $tanggalKe);
            //$dataHarian = array();

            if(count($dataHarian) > 0){
                $dataIsEmpty = false;
                if(count($dataHarian['tabel']) < 1){
                    $dataIsEmpty = true;
                }
            }

        }

        $array = array(
            'data_is_empty' => $dataIsEmpty,
            'tanggal' => $tanggalOri,
            'tanggal_ke' => $tanggalKeOri,
            'data_harian' => $dataHarian,
            'view_path' => $this->view_path,
            'master_blade' => $this->masterBlade,
            'view' => $this->view_path.'harian',
            'action' => Route::getCurrentRequest()->decodedPath(),
        );

        return $array;
    }

    private function getNeraca($tanggal){
        //return array();
        $now = strtotime(date('Y-m-d'));

        $debet = array();
        $kredit = array();
        $dataNeraca = array();

        if($tanggal == $now){
            $debet = $this->cashflow
                ->where('fungsi', '=', 'neraca')
                ->where('posisi', '=', 'D')
                ->orderBy('urutan', 'asc')->get()
            ;
            $kredit = $this->cashflow
                ->where('fungsi', '=', 'neraca')
                ->where('posisi', '=', 'K')
                ->orderBy('urutan', 'asc')->get()
            ;
        }

        $x = 1;
        $sumDebet = 0;
        $sumKredit = 0;

        foreach($debet as $db){
            $dataNeraca[$x] = array(
                'd_kode' => $db->kode,
                'd_keterangan' => $db->keterangan,
                'd_saldo' => $db->saldo,
                'k_kode' => '',
                'k_keterangan' => '',
                'k_saldo' => 0,
            );

            if($db->kode_head == '-'){
                $sumDebet += $db->saldo;
            }

            $x++;
        }

        $x = 1;


        foreach($kredit as $db){

            if(!isset($dataNeraca[$x])){
                $dataNeraca[$x] = array(
                    'd_kode' => '',
                    'd_keterangan' => '',
                    'd_saldo' => 0,
                    'k_kode' => '',
                    'k_keterangan' => '',
                    'k_saldo' => 0,
                );
            }

            $dataNeraca[$x]['k_kode'] = $db->kode;
            $dataNeraca[$x]['k_keterangan'] = $db->keterangan;
            $dataNeraca[$x]['k_saldo'] = $db->saldo;



            if($db->kode_head == '-'){
                $sumKredit += $db->saldo;
            }

            $x++;
        }

        return array(
            'tabel' => $dataNeraca,
            'sum_debet' => $sumDebet,
            'sum_kredit' => $sumKredit,
        );


    }

    private function getHarian($tanggal, $tanggalKe){
        if($tanggal + 86400 > $tanggalKe){
            $tanggalKe = $tanggal + 86400;
        }

        $tanggal = date('Y-m-d H:i:s', $tanggal);
        $tanggalKe = date('Y-m-d H:i:s', $tanggalKe);

        $tabel = $this->cashflowJurnal
            ->select('cashflow_jurnal_debet.id', 'cashflow_jurnal.id as jurnal_id', 'cashflow_jurnal.kode', 'username', 'keterangan', 'cashflow_jurnal_debet.kode as kode_cashflow', 'cashflow_jurnal_debet.nominal', 'cashflow_jurnal.last_update')
            ->join('cashflow_jurnal_debet', 'cashflow_jurnal.kode', '=', 'kode_jurnal')
            ->join('web_user', 'admin', '=', 'web_user.id')
            ->where('cashflow_jurnal.last_update', '<=', $tanggalKe)
            ->where('cashflow_jurnal.last_update', '>=', $tanggal)
            ->orderBy('cashflow_jurnal.id', 'desc')
            ->get();

        $arrTabel = array();

        foreach($tabel as $item){
            $detail = array(
                'kode_cashflow' => $item->kode_cashflow,
                'nominal' => $item->nominal,
            );

            if(!isset($arrTabel[$item->jurnal_id])){
                $arrTabel[$item->jurnal_id] = array(
                    'created_at' => $item->last_update,
                    'name' => $item->username,
                    'keterangan' => $item->keterangan,
                    'sum_d' => 0,
                    'sum_k' => 0,
                    'detail_d' => array(),
                    'detail_k' => array(),
                );
            }

            $arrTabel[$item->jurnal_id]['sum_d'] += 1;
            $arrTabel[$item->jurnal_id]['detail_d'][] = $detail;


        }

        $tabel = $this->cashflowJurnal
            ->select('cashflow_jurnal_kredit.id', 'cashflow_jurnal.id as jurnal_id', 'cashflow_jurnal.kode', 'username', 'keterangan', 'cashflow_jurnal_kredit.kode as kode_cashflow', 'cashflow_jurnal_kredit.nominal', 'cashflow_jurnal.last_update')
            ->join('cashflow_jurnal_kredit', 'cashflow_jurnal.kode', '=', 'kode_jurnal')
            ->join('web_user', 'admin', '=', 'web_user.id')
            ->where('cashflow_jurnal.last_update', '<=', $tanggalKe)
            ->where('cashflow_jurnal.last_update', '>=', $tanggal)
            ->orderBy('cashflow_jurnal.id', 'desc')
            ->get();

        //$arrTabel = array();

        foreach($tabel as $item){
            $detail = array(
                'kode_cashflow' => $item->kode_cashflow,
                'nominal' => $item->nominal,
            );

            if(!isset($arrTabel[$item->jurnal_id])){
                $arrTabel[$item->jurnal_id] = array(
                    'created_at' => $item->last_update,
                    'name' => $item->username,
                    'keterangan' => $item->keterangan,
                    'sum_d' => 0,
                    'sum_k' => 0,
                    'detail_d' => array(),
                    'detail_k' => array(),
                );
            }

            $arrTabel[$item->jurnal_id]['sum_k'] += 1;
            $arrTabel[$item->jurnal_id]['detail_k'][] = $detail;


        }

        return array(
            'tabel' => $arrTabel
        );
    }

    private function getLabaRugi($tanggal){
        $now = strtotime(date('Y-m-d'));

        $debet = array();
        $kredit = array();
        $dataNeraca = array();

        if($tanggal == $now){
            $debet = $this->cashflow
                ->where('fungsi', '=', 'labarugi')
                ->where('posisi', '=', 'D')
                ->orderBy('urutan', 'asc')->get()
            ;
            $kredit = $this->cashflow
                ->where('fungsi', '=', 'labarugi')
                ->where('posisi', '=', 'K')
                ->orderBy('urutan', 'asc')->get()
            ;
        }

        $x = 1;
        $sumDebet = 0;
        $sumKredit = 0;

        foreach($debet as $db){
            $dataNeraca[$x] = array(
                'd_kode' => $db->kode,
                'd_keterangan' => $db->keterangan,
                'd_saldo' => $db->saldo,
                'k_kode' => '',
                'k_keterangan' => '',
                'k_saldo' => 0,
            );

            if($db->kode_head == '-'){
                $sumDebet += $db->saldo;
            }

            $x++;
        }

        $x = 1;


        foreach($kredit as $db){

            if(!isset($dataNeraca[$x])){
                $dataNeraca[$x] = array(
                    'd_kode' => '',
                    'd_keterangan' => '',
                    'd_saldo' => 0,
                    'k_kode' => '',
                    'k_keterangan' => '',
                    'k_saldo' => 0,
                );
            }

            $dataNeraca[$x]['k_kode'] = $db->kode;
            $dataNeraca[$x]['k_keterangan'] = $db->keterangan;
            $dataNeraca[$x]['k_saldo'] = $db->saldo;



            if($db->kode_head == '-'){
                $sumKredit += $db->saldo;
            }

            $x++;
        }

        return array(
            'tabel' => $dataNeraca,
            'sum_debet' => $sumDebet,
            'sum_kredit' => $sumKredit,
        );;
    }
}