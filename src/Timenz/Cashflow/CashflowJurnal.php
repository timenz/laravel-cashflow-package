<?php  namespace Timenz\Cashflow;

use Eloquent;

class CashflowJurnal extends Eloquent{
    protected $fillable = array('admin', 'keterangan', 'nominal', 'kode');
    protected $table = 'cashflow_jurnal';
    public $timestamps = false;
}