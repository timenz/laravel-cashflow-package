<?php  namespace Timenz\Cashflow;

use DB;

class CashflowModel{

    protected $cashflow;
    protected $cashflowJurnal;
    protected $cashflowJurnalDebet;
    protected $cashflowJurnalKredit;

    protected $arrJurnal;
    protected $nominal;
    protected $keterangan;
    protected $userId;
    protected $kodeJurnal;

    public function __construct(){
        $this->cashflow = new CashflowTable();
        $this->cashflowJurnal = new CashflowJurnal();
        $this->cashflowJurnalKredit = new CashflowJurnalKredit();
        $this->cashflowJurnalDebet = new CashflowJurnalDebet();
    }


    public function inputJurnal($arrJurnal, $nominal, $keterangan, $userId, $kodeJurnal = ''){
        $this->arrJurnal = $arrJurnal;
        $this->nominal = $nominal;
        $this->keterangan = $keterangan;
        $this->userId = $userId;


        if($kodeJurnal == ''){
            $kodeJurnal = 'n1';
            $row = $this->cashflowJurnal->orderBy('id', 'desc')->first();
            if($row != null){
                $id = $row->id + 1;
                $kodeJurnal = 'n'.$id;
            }
        }
        $this->kodeJurnal = $kodeJurnal;

        $use = $this;

        $insert_id = DB::transaction(function() use($use) {
            $insert_id = $this->cashflowJurnal->create(array(
                'admin' => $use->userId,
                'keterangan' => $use->keterangan,
                'nominal' => $use->nominal,
                'kode' => $use->kodeJurnal,
            ))->getAttribute('id');
            $arrayDebet = array();
            $arrayKredit = array();
            $valid = true;
            foreach($use->arrJurnal as $item){

                $updateCasflow = $this->updateCashflow($item['kode'], $item['nominal'], $item['posisi']);

                if(!$updateCasflow){
                    $valid = false;
                    break;
                }

                if($item['posisi'] == 'D'){
                    $arrayDebet[] = array(
                        'kode_jurnal' => $this->kodeJurnal,
                        'kode' => $item['kode'],
                        'nominal' => $item['nominal']
                    );
                }

                if($item['posisi'] == 'K'){
                    $arrayKredit[] = array(
                        'kode_jurnal' => $this->kodeJurnal,
                        'kode' => $item['kode'],
                        'nominal' => $item['nominal']
                    );
                }




            }

            if(count($arrayDebet) < 1 or count($arrayKredit) < 1){
                $valid = false;
            }

            $this->cashflowJurnalDebet->insert($arrayDebet);
            $this->cashflowJurnalKredit->insert($arrayKredit);

            if(!$valid){
                DB::rollBack();
            }



            return $insert_id;
        });

        return $insert_id;
    }

    private function updateCashflow($kodeJurnal, $nominal, $posisi){
        $arrCode = $this->recursiveCode($kodeJurnal);

        $cashflowIn = $this->cashflow->whereIn('kode', $arrCode)->get();

        if(count($arrCode) != count($cashflowIn)){
            return false;
        }

        foreach ($cashflowIn as $item) {

            if($item->posisi == 'D'){
                if($posisi == 'D'){
                    $nominalUpdate = $item->saldo + $nominal;
                }else{
                    $nominalUpdate = $item->saldo - $nominal;
                }
            }else{
                if($posisi == 'D'){
                    $nominalUpdate = $item->saldo - $nominal;
                }else{
                    $nominalUpdate = $item->saldo + $nominal;
                }
            }

            $this->cashflow->where(array('id' => $item->id))->update(array('saldo' => $nominalUpdate));
        }

        return true;
    }

    private function recursiveCode($string, $out = array()){

        $explode = explode('.', $string);

        if(count($out) == 0){
            $out[] = $string;
        }


        if(count($explode) > 0){
            array_pop($explode);
            $imp = implode('.', $explode);
            if($imp != ''){
                $out[] = $imp;
            }


            if(count($explode) > 0){
                $out = $this->recursiveCode($imp, $out);
            }
        }

        return $out;
    }

    public function getStringBetween($string, $start, $end) {
        $string = " " . $string;
        $ini = strpos($string, $start);
        if ($ini == 0)
            return "";
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }
}