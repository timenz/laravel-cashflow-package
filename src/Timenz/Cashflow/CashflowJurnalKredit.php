<?php  namespace Timenz\Cashflow;
use Eloquent;

class CashflowJurnalKredit extends Eloquent{
    protected $fillable = array('kode_jurnal', 'kode', 'nominal');
    protected $table = 'cashflow_jurnal_kredit';
    public $timestamps = false;

}