<?php namespace Timenz\Cashflow;

class Cashflow {

    private $arrayDebet = array();
    private $arrayKredit = array();

    public function addDebet($kode, $nominal){
        $this->arrayDebet[] = array(
            'kode' => $kode,
            'nominal' => $nominal,
            'posisi' => 'D',
        );
    }

    public function addKredit($kode, $nominal){
        $this->arrayKredit[] = array(
            'kode' => $kode,
            'nominal' => $nominal,
            'posisi' => 'K',
        );
    }

    public function reset(){
        $this->arrayDebet = array();
        $this->arrayKredit = array();
    }

    public function insertJurnal($nominal, $keterangan, $id_user, $kodeJurnal = ''){

        if(count($this->arrayKredit) < 1 or count($this->arrayDebet) < 1){
            return false;
        }

        $dataJurnal = array_merge($this->arrayDebet, $this->arrayKredit);

        $cashflowModel = new CashflowModel();
        $status = $cashflowModel->inputJurnal($dataJurnal, $nominal, $keterangan, $id_user, $kodeJurnal);


        return $status;
    }

    public static function neraca($masterBlade){
        $data = new CashflowData();
        $data->setMasterBlade($masterBlade);
        return $data->neraca();
    }

    public static function labaRugi($masterBlade){
        $data = new CashflowData();
        $data->setMasterBlade($masterBlade);
        return $data->labaRugi();
    }

    public static function jurnal($masterBlade, $iduser){

        $data = new CashflowData();
        $data->setMasterBlade($masterBlade);
        $data->setIdUser($iduser);
        return $data->jurnal();
    }

    public static function harian($masterBlade){

        $data = new CashflowData();
        $data->setMasterBlade($masterBlade);
        return $data->harian();
    }



}