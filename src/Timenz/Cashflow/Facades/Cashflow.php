<?php namespace Timenz\Cashflow\Facades;

use Illuminate\Support\Facades\Facade;

class Cashflow extends Facade {

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'cashflow'; }

}