
<div class="col-sm-6">
    <ul class="nav nav-tabs">
      <li class="{{ $cashflow['act_neraca'] }}"><a href="{{ url('cashflow_report') }}">Neraca</a></li>
      <li class="{{ $cashflow['act_labarugi'] }}"><a href="{{ url('cashflow_report_labarugi') }}">Laba Rugi</a></li>
      <li class="{{ $cashflow['act_harian'] }}"><a href="{{ url('cashflow_report_harian') }}">Harian</a></li>
      <li class="{{ $cashflow['act_akun'] }}"><a href="{{ url('cashflow_report_akun') }}">Akun</a></li>
      <li class="{{ $cashflow['act_jurnal'] }}"><a href="{{ url('cashflow_jurnal') }}">Jurnal</a></li>
    </ul>
</div>
