@extends($cashflow['master_blade'])

@section('konten')
    <div class="row">
        {{--@include('admin.cashflow_report.submenu')--}}
        <div class="col-sm-12">
            <form class="form-inline pull-right" role="form" method="get" action="{{ url($cashflow['action']) }}">
              <div class="form-group">
                <label class="sr-only" for="tanggal">Tanggal</label>
                <input type="text" class="form-control" id="tanggal" name="tanggal" placeholder="Tanggal" value="{{ $cashflow['tanggal'] }}" >
              </div>
              <div class="form-group">
                <label class="sr-only" for="tanggal_ke">Tanggal Ke</label>
                <input type="text" class="form-control" id="tanggal_ke" name="tanggal_ke" placeholder="Tanggal Ke" value="{{ $cashflow['tanggal_ke'] }}" >
              </div>
              <button type="submit" class="btn btn-default">Cari</button>
            </form>
        </div>
    </div>
    <div class="clearfix" style="height: 20px;"></div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Laporan Harian</h3>
                </div>
                <div class="panel-body">
                    @if($cashflow['data_is_empty'])
                    <p>Silakan pilih tanggal kemudian klik cari.</p>

                    @else

                    <table class="table table-bordered">
                    <tr>
                        <th rowspan="2" class="">Tanggal</th>
                        <th rowspan="2">Keterangan</th>
                        <th colspan="2" class="text-center">Debet</th>
                        <th colspan="2" class="text-center">Kredit</th>
                        <th rowspan="2">Admin</th>
                    </tr>
                    <tr>
                        <th>Kode</th>
                        <th>Nominal</th>
                        <th>Kode</th>
                        <th>Nominal</th>
                    </tr>

                    @foreach($cashflow['data_harian']['tabel'] as $item)
                    <?php $span = $item['sum_d'] > $item['sum_k'] ? $item['sum_d'] : $item['sum_k']; ?>
                    <tr>
                        <td rowspan="{{ $span }}">{{ $item['created_at'] }}</td>
                        <td rowspan="{{ $span }}">{{ $item['keterangan'] }}</td>
                        @foreach($item['detail_d'] as $subitem)
                            <td>{{ $subitem['kode_cashflow'] }}</td>
                            <td class="text-right">{{ number_format($subitem['nominal'], 2) }}</td>
                            <?php break; ?>

                        @endforeach
                        @foreach($item['detail_k'] as $subitem)
                            <td>{{ $subitem['kode_cashflow'] }}</td>
                            <td class="text-right">{{ number_format($subitem['nominal'], 2) }}</td>
                            <?php break; ?>

                        @endforeach
                        <td rowspan="{{ $span }}">{{ $item['name'] }}</td>

                    </tr>
                    @if($span > 1)
                        @for($i=1; $i<$span;$i++)
                            <tr>
                            @if(isset($item['detail_d'][$i]))
                                <td>{{ $item['detail_d'][$i]['kode_cashflow'] }}</td>
                                <td class="text-right">{{ number_format($item['detail_d'][$i]['nominal'], 2) }}</td>
                            @else
                                <td></td>
                                <td></td>
                            @endif
                            @if(isset($item['detail_k'][$i]))
                                <td>{{ $item['detail_k'][$i]['kode_cashflow'] }}</td>
                                <td class="text-right">{{ number_format($item['detail_k'][$i]['nominal'], 2) }}</td>
                            @else
                                <td></td>
                                <td></td>
                            @endif
                            </tr>
                        @endfor
                    @endif

                    @endforeach

                    </table>

                    @endif


                </div>
            </div>
        </div>

    </div>

@stop

@section('js')
    <script>
    $(function(){
        $('#tanggal').datepicker({
            format: 'dd-mm-yyyy',
            endDate: '+0d',
            autoclose: true

        });
        $('#tanggal_ke').datepicker({
            format: 'dd-mm-yyyy',
            endDate: '+2d',
            autoclose: true

        });
    });
    </script>
@stop