@extends($cashflow['master_blade'])

@section('konten')
    <div class="row">
        {{--@include('admin.cashflow_report.submenu')--}}
        <div class="col-sm-12">
            <form class="form-inline pull-right" role="form" method="get" action="{{ url($cashflow['action']) }}">
              <div class="form-group">
                <label class="sr-only" for="tanggal">Tanggal</label>
                <input type="text" class="form-control" id="tanggal" name="tanggal" placeholder="Tanggal" value="{{ $cashflow['tanggal'] }}" >
              </div>
              <button type="submit" class="btn btn-default">Cari</button>
            </form>
        </div>
    </div>
    <div class="clearfix" style="height: 20px;"></div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Laporan Laba Rugi</h3>
                </div>
                <div class="panel-body">
                    @if($cashflow['data_is_empty'])
                    <p>Silakan pilih tanggal kemudian klik cari.</p>

                    @else

                    <table class="table table-bordered">
                    <tr>
                        <th colspan="4" class="text-center">PENDAPATAN</th>
                        <th colspan="4" class="text-center">PENGELUARAN</th>
                    </tr>
                    <tr>
                        <th>No</th>
                        <th>Kode Akun</th>
                        <th>Keterangan</th>
                        <th>Saldo</th>
                        <th>No</th>
                        <th>Kode Akun</th>
                        <th>Keterangan</th>
                        <th>Saldo</th>
                    </tr>

                    @foreach($cashflow['data_labarugi']['tabel'] as $key => $item)
                    <tr>
                        <td>{{ $key }}</td>
                        <td>{{ $item['k_kode'] }}</td>
                        <td>{{ $item['k_keterangan'] }}</td>
                        <td class="text-right">{{ number_format($item['k_saldo'], 2) }}</td>
                        <td>{{ $key }}</td>
                        <td>{{ $item['d_kode'] }}</td>
                        <td>{{ $item['d_keterangan'] }}</td>
                        <td class="text-right">{{ number_format($item['d_saldo'], 2) }}</td>
                    </tr>
                    @endforeach
                    <tr>
                    <td colspan="3">Total</td>
                    <td class="text-right">{{ number_format($cashflow['data_labarugi']['sum_kredit'], 2) }}</td>
                    <td colspan="3">Total</td>
                    <td class="text-right">{{ number_format($cashflow['data_labarugi']['sum_debet'], 2) }}</td>
                    </tr>
                    </table>

                    @endif


                </div>
            </div>
        </div>

    </div>

@stop

@section('js')
    <script>
    $(function(){
        $('#tanggal').datepicker({
            format: 'dd-mm-yyyy',
            endDate: '+0d',
            autoclose: true

        });
    });
    </script>
@stop