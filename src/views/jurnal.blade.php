@extends($cashflow['master_blade'])

@section('konten')
<div class="row">
    {{--@include($cashflow['view_path'].'submenu')--}}
    <div class="col-md-6"></div>
</div>
<div class="clearfix" style="height: 20px;"></div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Cashflow Jurnal</h3>

            </div>

            <div class="panel-body">
                {{ Form::open(array('url' => 'cashflow_jurnal', 'method' => 'post', 'id' => 'form_cashflow')) }}
                <table id="table-cashflow" class="table table-bordered ">
                    <tr><td>Keterangan</td><td colspan="3"><input name="keterangan" type="text" class="form-control"></td></tr>
                    <tr><td colspan="2" class="text-center">Debet</td><td colspan="2" class="text-center">Kredit</td></tr>
                    <tr class="tr-row tr-1" data-no="1">
                        <td style="width: 30%"><input name="kode-debet[]" type="text" class="form-control kode-debet-1 input-select-all input-kode"></td>
                        <td><input name="nom-debet[]" type="text" class="form-control"></td>
                        <td style="width: 30%"><input name="kode-kredit[]" type="text" class="form-control kode-kredit-1 input-select-all input-kode"></td>
                        <td><input name="nom-kredit[]" type="text" class="form-control"></td>
                    </tr>
                    <tr class="tr-row tr-2" data-no="2">
                        <td style="width: 30%"><input name="kode-debet[]" type="text" class="form-control kode-debet-2 input-select-all input-kode"></td>
                        <td><input name="nom-debet[]" type="text" class="form-control"></td>
                        <td style="width: 30%"><input name="kode-kredit[]" type="text" class="form-control kode-kredit-2 input-select-all input-kode"></td>
                        <td><input name="nom-kredit[]" type="text" class="form-control"></td>
                    </tr>

                </table>
                <div class="col-sm-12 error"></div>
                <input type="hidden" name="id_user" value="{{ $cashflow['id_user'] }}">
                <input type="submit" value="Simpan" class="btn btn-primary pull-right" id="submit" >
                </form>
            </div>
        </div>
    </div>

</div>
@stop

@section('js')
    <script>
    var debet = {{ $cashflow['debet'] }};
    var kredit = {{ $cashflow['kredit'] }};
    var kodeJurnal = {{ $cashflow['kodeJurnal'] }};

    $(function(){
        bind_typeahead(1);
        bind_typeahead(2);

        $('#submit').click(function(e){

            var data = $('#form_cashflow').serializeArray(),
            post_data = form_serialize_convert('form_cashflow'), sum_debet = 0, sum_kredit = 0;

            for(i in data){
                var value = parseFloat(data[i].value);
                if(isNaN(value)){continue;}
                if(data[i].name == 'nom-debet[]'){
                    sum_debet += value
                }
                if(data[i].name == 'nom-kredit[]'){
                    sum_kredit += value
                }

            }

            if(post_data.keterangan == ''){
                $('.error').html('<p>Keterangan Jurnal tolong di isi.</p>');
                e.preventDefault();
                return;
            }

            if(sum_debet == 0 || sum_kredit == 0){
                $('.error').html('<p>Isi nominal debet dan atau kredit dengan benar.</p>');
                e.preventDefault();
                return;
            }

            if(sum_debet != sum_kredit){
                $('.error').html('<p>Total debet dan kredit tidak seimbang.</p>');
                e.preventDefault();
                return;
            }


        });

        $('#table-cashflow').on('keyup', '.input-kode', function(){
            var click_no = $(this).parents('tr').attr('data-no');
            var tr_count = $('.tr-row').length;

            if(tr_count <= click_no){
                var new_tr = tr_count + 1;
                str = '' +
                 '<tr class="tr-row tr-'+new_tr+'" data-no="'+new_tr+'">' +
                 '      <td style="width: 30%"><input name="kode-debet[]" type="text" class="form-control kode-debet-'+new_tr+' input-select-all input-kode"></td>' +
                 '      <td><input name="nom-debet[]" type="text" class="form-control"></td>' +
                 '      <td style="width: 30%"><input name="kode-kredit[]" type="text" class="form-control kode-kredit-'+new_tr+' input-select-all input-kode"></td>' +
                 '      <td><input name="nom-kredit[]" type="text" class="form-control"></td>' +
                 '  </tr>';

                 $('#table-cashflow').append(str);
                 bind_typeahead(new_tr);
            }

        });


    });

    function bind_typeahead(no){
        $('.kode-debet-' + no).typeahead({
            hint: 'true',
            highlight: 'true',
            minLength: 1
        },
        {
            name: 'debet',
            displayKey: 'value',
            source: substringMatcher(kodeJurnal)
        });
        $('.kode-kredit-' + no).typeahead({
            hint: 'true',
            highlight: 'true',
            minLength: 1
        },
        {
            name: 'debet',
            displayKey: 'value',
            source: substringMatcher(kodeJurnal)
        });
    }
    </script>
@stop