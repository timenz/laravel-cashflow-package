<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashflowTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cashflow', function(Blueprint $table){
			$table->increments('id');
			$table->string('kode', 20)->unique();
			$table->string('kode_head', 20);
			$table->string('keterangan', 120);
			$table->decimal('saldo', 17, 2);
			$table->enum('posisi', array('D', 'K'));
			$table->enum('fungsi', array('neraca', 'labarugi'));
			$table->integer('view');
			$table->integer('urutan');
			$table->timestamps();

		});

		Schema::create('cashflow_eod', function(Blueprint $table){
			$table->increments('id');
			$table->integer('cashflow_id')->unsigned();
			$table->foreign('cashflow_id')->references('id')->on('cashflow')->onDelete('cascade');
			$table->decimal('saldo', 17, 2);
			$table->timestamps();
		});

		Schema::create('cashflow_jurnal', function(Blueprint $table){
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			$table->string('keterangan', 200);
			$table->decimal('nominal', 10, 2);
			$table->timestamps();

		});

		Schema::create('cashflow_jurnal_detail', function(Blueprint $table){
			$table->increments('id');
			$table->integer('jurnal_id')->unsigned();
			$table->foreign('jurnal_id')->references('id')->on('cashflow_jurnal')->onDelete('cascade');
			$table->string('kode_cashflow', 20);
			$table->foreign('kode_cashflow')->references('kode')->on('cashflow')->onDelete('cascade');
			$table->decimal('nominal', 10, 2);
			$table->enum('posisi', array('D', 'K'));

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('cashflow_jurnal_detail');
		Schema::dropIfExists('cashflow');
		Schema::dropIfExists('cashflow_jurnal');
	}

}
