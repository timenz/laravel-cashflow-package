##Laravel Cashflow Package

Paket untuk menyediakan fungsi cashflow, ui, dan reportnya

composer setting

     {
         "require": {
             "timenz/cashflow": "dev-master"
         },
         "repositories": {
             "vcs_crud": {
               "type": "vcs",
               "url": "https://bitbucket.org/timenz/laravel-cashflow-package.git"
             }
         },
     }


migrate db

     php artisan migrate --bench="timenz/cashflow"
     php artisan db:seed --class="CashflowTableSeeder"

contoh controller

     public function neraca(){
          $cashflow = Cashflow::neraca('admin.master');
          $data = array(
               'title' => 'Test Cashflow',
               'cashflow' => $cashflow
          );

          return View::make($cashflow['view'], $data);
     }
     
publish views

    php artisan view:publish --path="workbench/timenz/cashflow/src/views" timenz/cashflow
    php artisan view:publish timenz/cashflow
    